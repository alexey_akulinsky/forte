// Initialize your app
var app = new Framework7();

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = app.addView('.view-main', {
    dynamicNavbar: true
});

var octaves = [ 'Контроктава', 'Большая октава', 'Малая октава', 'Первая октава', 'Вторая октава', 'Третья октава' ];
var notes = [ 'До', 'Ре', 'Ми', 'Фа', 'Соль', 'Ля', 'Си' ];

var getNotePosition = function(value, min, max, step) {
    var floatingPosition = Math.floor(value * (max - min + 1)) + min;
    return Math.floor(floatingPosition / step) - 1;
}

var getNote = function(notePosition) {
    var reversedOctaves = octaves.slice().reverse();
    var reversedNotes = notes.slice().reverse();

    var currentOctave = Math.floor((notePosition) / 7);
    var currentNote = reversedNotes[notePosition - currentOctave * 7];

    return currentNote + ' (' + reversedOctaves[currentOctave] + ')';
}

var getNotesByOctave = function(inputName) {
    return $$("div[id='" + inputName + "'] input");
}

app.selectAll = function(el) {
    setTimeout(function() {
        var input = el.getElementsByTagName('input')[0];
        var checked = input.checked;
        var noteEls = getNotesByOctave(input.name);
        $$.each(noteEls, function(index, el) {
            el.checked = checked;
        });
    }, 1);
}

var notePosition = 17;
$$('#noteBtn')[0].onclick = function() {
    if (!app.notesToTraining || app.notesToTraining.length == 0) {
        return;
    }
    var currentNote = document.getElementById('notebook').getSVGDocument().getElementById('path2665');
    var step = 50;
    notePosition = null;
    while (notePosition === null) {
        var generatedPos = getNotePosition(Math.random(), 50, 2150, step);
        if (app.notesToTraining.indexOf(generatedPos) != -1) {
            notePosition = generatedPos;
        }
    }
    currentNote.pathSegList[0].y = notePosition * step + step;
};

$$('#answerBtn')[0].onclick = function() {
    app.alert(getNote(notePosition), '');
};

app.onBackBtnClick = function() {
    var arr = app.notesToTraining = [];
    var reversedOctaveArr = octaves.slice().reverse();
    $$.each(reversedOctaveArr, function(octIndex, octave) {
        var octave = octave;
        var reversedNoteEls = [];
        var noteEls = getNotesByOctave(octave);
        noteEls.each(function(index, el) {
            reversedNoteEls.push(el);
        });
        reversedNoteEls.reverse();
        $$.each(reversedNoteEls, function(noteIndex, el) {
            if (el.checked) {
                arr.push(octIndex * 7 + noteIndex);
            }
        });
    });
};

app.onPageInit('settings', function(page) {
    var settingsEl = $$('.octave-settings')[0];
    var octaveTemplate = $$('#octavesTemplate').html();
    settingsEl.innerHTML = Mustache.render(octaveTemplate, {
        octaves: octaves,
        notes: notes
    });
});
